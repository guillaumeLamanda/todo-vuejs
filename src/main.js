import Vue from "vue"
import ElementUI from "element-ui"
import "element-ui/lib/theme-chalk/index.css"

import App from "./App.vue"
Vue.use(ElementUI)

import apiPlugin from "./plugins/api"
Vue.use(apiPlugin, { uri: "http://localhost:3000/api" })

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount("#app")
