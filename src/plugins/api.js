import { create } from "axios"

export default {
  install: (Vue, { uri }) => {
    console.log(uri)
    Vue.prototype.api = create({
      baseURL: uri
    })
  }
}
